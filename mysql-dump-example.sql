CREATE TABLE IF NOT EXISTS `colors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(10) unsigned NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;


INSERT INTO `colors` (`id`, `product_id`, `name`) VALUES
	(1, 1, 'черный'),
	(2, 1, 'серебристый'),
	(3, 1, 'красный'),
	(4, 1, 'белый'),
	(5, 2, 'розовый'),
	(6, 2, 'черный'),
	(7, 2, 'белый'),
	(8, 2, 'серебро'),
	(9, 3, 'синий'),
	(10, 3, 'радужный'),
	(11, 3, 'сахарный пони'),
	(12, 4, 'белый'),
	(13, 4, 'черный'),
	(14, 4, 'синий'),
	(15, 5, 'рыжий'),
	(16, 5, 'блонд'),
	(17, 5, 'пепельный'),
	(18, 6, 'белый'),
	(19, 6, 'серебристый'),
	(20, 6, 'черный'),
	(21, 6, 'красный');

CREATE TABLE IF NOT EXISTS `customers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `city` varchar(50) NOT NULL DEFAULT '',
  `address` varchar(50) NOT NULL DEFAULT '',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;


INSERT INTO `customers` (`id`, `name`, `city`, `address`, `updated_at`, `created_at`) VALUES
	(1, 'Дарт Вейдер', 'Иркутск', 'Лермонтова, 126, ауд. 245', '2015-10-03 09:21:36', '2015-10-03 09:20:55'),
	(2, 'Спанч Боб', 'Дно океана', 'самое дно', '2015-10-03 09:22:05', '2015-10-03 09:22:05');


CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(10) unsigned NOT NULL,
  `status` enum('OPEN','PROCESSING','COMPLETED') NOT NULL DEFAULT 'OPEN',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;


INSERT INTO `orders` (`id`, `customer_id`, `status`, `updated_at`, `created_at`) VALUES
	(1, 1, 'OPEN', '2015-10-03 09:25:15', '2015-10-03 09:25:15');


CREATE TABLE IF NOT EXISTS `order_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `color_id` int(10) unsigned NOT NULL,
  `count` int(10) unsigned NOT NULL DEFAULT '1',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;


INSERT INTO `order_items` (`id`, `order_id`, `product_id`, `color_id`, `count`, `updated_at`, `created_at`) VALUES
	(1, 1, 1, 3, 1, '2015-10-03 09:58:18', '2015-10-03 09:28:14'),
	(2, 1, 4, 14, 2, '2015-10-03 09:29:44', '2015-10-03 09:29:44');


CREATE TABLE IF NOT EXISTS `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(500) DEFAULT NULL,
  `price` decimal(10,0) DEFAULT '0',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;


INSERT INTO `products` (`id`, `name`, `price`, `updated_at`, `created_at`) VALUES
	(1, 'Ноутбук', 30000, '2015-10-03 09:09:55', '2015-10-03 09:09:55'),
	(2, 'Телефон', 5000, '2015-10-03 09:10:09', '2015-10-03 09:10:09'),
	(3, 'Кофта', 3000, '2015-10-03 09:10:21', '2015-10-03 09:10:22'),
	(4, 'Кроссовки', 5000, '2015-10-03 09:10:54', '2015-10-03 09:10:54'),
	(5, 'Парик', 10000, '2015-10-03 09:11:08', '2015-10-03 09:11:08'),
	(6, 'Холодильник', 50000, '2015-10-03 09:11:31', '2015-10-03 09:11:31');

CREATE TABLE IF NOT EXISTS `reviews` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `comment` text NOT NULL,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;


INSERT INTO `reviews` (`id`, `customer_id`, `product_id`, `comment`, `updated_at`, `created_at`) VALUES
	(1, 1, 4, 'Отличный кроссовка, понимаэш! Рекомендую! Доволен как слон!', '2015-10-03 09:32:03', '2015-10-03 09:31:23'),
	(2, 2, 1, 'Ноут не очень, под водой сдох сразу же!!!', '2015-10-03 09:33:13', '2015-10-03 09:32:54');
